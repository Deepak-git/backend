const router = require('express').Router();
const userController = require('../controller/controller');
const auth = require('../helper/helper');


router.post('/signUp', userController.signUp);
router.post('/login', userController.login);

// This is protected route only of login user assess this route
router.post('/addState',auth.verifyToken, userController.addState);
router.post('/addDistrict',auth.verifyToken, userController.addDistrict);
router.get('/getState',auth.verifyToken, userController.getState);
router.get('/getDistrict', auth.verifyToken,userController.getDistrict);
router.post('/addChild',auth.verifyToken, userController.addChild);
router.get('/viewChild',auth.verifyToken, userController.viewChild);
router.post('/logout',auth.verifyToken,userController.logout);







module.exports = router