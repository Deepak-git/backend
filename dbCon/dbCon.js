const mongoose = require('mongoose');

// Db connection start heare for localhost
mongoose.connect('mongodb://localhost:27017/testdb',{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
},(error)=>{
    if(error){
        console.log(error)
    }else{
        console.log('db connected successfully')
    }
})