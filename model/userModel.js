const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
    
     userName:{
        type:String,
        required:true,
        unique:true,
    },

    userStatus:{
    type:String,
    enum:["ACTIVE","BLOCK","DELETE"],
    default:"ACTIVE"
    },

    userType:{
        type:String,
        enum:["ADMIN","USER"],
        default:"USER"
    },

    userPassword:{
        type:String,
        required:true
    },
    logo:{
        type:String,
        default:''
     },
     organization:{
         type:String,
         enum:["Bal Vikas","Bal Kalyan"],
         default:"Bal Vikas"
     },
     designation:{
         type:String,
         enum:["Cluster Coordinator","Associate Consultant"],
         dafault:"Cluster Coordinator"
     },
     backgroundImage:{
         type:String,
         default:''
     },
     stateName:{
         type:Array
     },
     districtName:{
         type:Array
     },
        
},{timestamps:true})

module.exports = mongoose.model('userModel',userSchema)