const mongoose = require('mongoose');
const childSchema = new mongoose.Schema({
    name:{
        type:String
    },
    sex:{
        type:String,
        enum:["MALE","FEMALE"]
    },
    dateOfBirth:{
        type:String
    },
    fatherName:{
        type:String
    },
    motherName:{
        type:String
    },
    state:{
        type:String
    },
    district:{
        type:String
    },
     profileImege:{
     type:String,
     default:""
     }
    

},{timestamps:true})
module.exports = mongoose.model('childModel',childSchema)