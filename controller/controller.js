const userModel = require("../model/userModel");
const childModel = require("../model/childModel");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

module.exports = {
  signUp: (req, res) => {
    try {
      if (!req.body.userName) {
        return res
          .status(404)
          .send({ status: 0, messege: "user name required" });
      } else if (!req.body.userPassword) {
        return res
          .status(404)
          .send({ status: 0, messege: "userPassword required" });
      } else {
        userModel.findOne(
          { userName: req.body.userName, userStatus: { $in: ["ACTIVE"] } },
          (findErr, findRes) => {
            if (findErr) {
              return res
                .status(500)
                .send({ status: 0, messege: "internal server error" });
            } else if (findRes) {
              return res
                .status(509)
                .send({ status: 0, messege: "user already exist" });
            } else {
              bcrypt.hash(req.body.userPassword, 10, (err, hash) => {
                if (err) {
                  return res
                    .status(501)
                    .send({ status: 0, messege: "some error occured" });
                } else {
                  var data = {
                    userName: req.body.userName,
                    userPassword: hash,
                    userType: req.body.userType,
                  };
                  new userModel(data).save((saveErr, saveRes) => {
                    if (saveErr) {
                      return res
                        .status(500)
                        .send({ status: 0, messege: "internal server error" });
                    } else if (!saveRes) {
                      return res
                        .status(503)
                        .send({ status: 0, messege: "error in register user" });
                    } else {
                      return res.status(200).send({
                        status: 1,
                        messege: "signUp successfull Please login !!!",
                      });
                    }
                  });
                }
              });
            }
          }
        );
      }
    } catch (error) {
      return res
        .status(501)
        .send({ status: 0, messege: "Tempory error occured please try again" });
    }
  },
  // This is end of signup
  login: (req, res) => {
    try {
      if (!req.body.userName) {
        return res
          .status(404)
          .send({ status: 0, messege: "userName required" });
      } else if (!req.body.userPassword) {
        return res
          .status(404)
          .send({ status: 0, messege: "userPassword required" });
      } else {
        userModel.findOne(
          { userName: req.body.userName, userStatus: { $in: ["ACTIVE"] } },
          (findErr, findRes) => {
            if (findErr) {
              return res
                .status(500)
                .send({ status: 0, messege: "internal server error" });
            } else if (!findRes) {
              return res
                .status(404)
                .send({ status: 0, messege: "user not exist please signup" });
            } else {
              var check = bcrypt.compareSync(
                req.body.userPassword,
                findRes.userPassword
              );
              if (check === true) {
                jwt.sign(
                  { _id: findRes._id },
                  "SECREATEKEYJWTTESTING",
                  { expiresIn: "1h" },
                  (error, token) => {
                    if (error) {
                      return res
                        .status(500)
                        .send({ status: 0, messege: "internal server error" });
                    } else {
                      var name = findRes.userName,
                        organization = findRes.organization,
                        designation = findRes.designation,
                        backgroundImage = findRes.backgroundImage;
                      return res.status(200).send({
                        status: 1,
                        messege: "login successfull",
                        token,
                        name,
                        organization,
                        designation,
                        backgroundImage,
                      });
                    }
                  }
                );
              } else {
                return res.status(409).send({
                  status: 0,
                  messege: "userName or userPassword worng !!!",
                });
              }
            }
          }
        );
      }
    } catch (error) {
      return res
        .status(501)
        .send({ status: 0, messege: "Temporery error occured" });
    }
  },
  //   This is end of login
  addState: (req, res) => {
    try {
      if (!req.body.stateName) {
        return res
          .status(404)
          .send({ status: 0, messege: "stateName required" });
      } else if (!req.user) {
        return res
          .status(400)
          .send({ status: 0, messege: "user login required" });
      } else {
        userModel.findOne({ _id: req.user._id }, (findErr, findRes) => {
          if (findErr) {
            return res
              .status(500)
              .send({ status: 0, messege: "internal server error" });
          } else if (!findRes) {
            return res
              .status(404)
              .send({ status: 0, messege: "user not found" });
          } else {
            userModel.findByIdAndUpdate(
              { _id: findRes._id },
              { $push: { stateName: req.body.stateName } },
              { new: true },
              (upErr, upRes) => {
                if (upErr) {
                  return res
                    .status(500)
                    .send({ status: 0, messege: "internal server error" });
                } else if (!upRes) {
                  return res
                    .status(409)
                    .send({ status: 0, messege: "error in adding state" });
                } else {
                  return res
                    .status(200)
                    .send({ status: 1, messege: "sate added successfully" });
                }
              }
            );
          }
        });
      }
    } catch (error) {
      return res
        .send(409)
        .send({ status: 0, messege: "temporery error ocured" });
    }
  },
  //  this is end of add state
  addDistrict: (req, res) => {
    try {
      if (!req.body.districtName) {
        return res
          .status(404)
          .send({ status: 0, messege: "districtName required" });
      } else if (!req.user) {
        return res
          .status(400)
          .send({ status: 0, messege: "user login required" });
      } else {
        userModel.findOne({ _id: req.user._id }, (findErr, findRes) => {
          if (findErr) {
            return res
              .status(500)
              .send({ status: 0, messege: "internal server error" });
          } else if (!findRes) {
            return res
              .status(404)
              .send({ status: 0, messege: "user not found" });
          } else {
            userModel.findByIdAndUpdate(
              { _id: findRes._id },
              { $set: { districtName: req.body.districtName } },
              { new: true },
              (upErr, upRes) => {
                if (upErr) {
                  return res
                    .status(500)
                    .send({ status: 0, messege: "internal server error" });
                } else if (!upRes) {
                  return res
                    .status(409)
                    .send({ status: 0, messege: "error in adding district" });
                } else {
                  return res
                    .status(200)
                    .send({
                      status: 1,
                      messege: "district added successfully",
                    });
                }
              }
            );
          }
        });
      }
    } catch (error) {
      return res
        .send(409)
        .send({ status: 0, messege: "temporery error ocured" });
    }
  },
  // this is end of add District
  getState: (req, res) => {
    try {
      if (!req.user) {
        return res
          .status(400)
          .send({ status: 0, messege: "user login required" });
      } else {
        userModel
          .findOne({ stateName: req.user.stateName })
          .select({ stateName: 1, _id: 0 })
          .exec((findErr, findRes) => {
            if (findErr) {
              return res
                .status(500)
                .send({ status: 0, messege: "internal server error" });
            } else if (!findRes) {
              return res
                .status(404)
                .send({ status: 0, messege: "state not exist" });
            } else {
              return res
                .status(200)
                .send({
                  status: 1,
                  messege: "state fatch successfully",
                  findRes,
                });
            }
          });
      }
    } catch (error) {
      return res
        .status(409)
        .send({ status: 0, messege: "Temporerty error occured" });
    }
  },
  // This is end of get state

  getDistrict: (req, res) => {
    try {
      if (!req.user) {
        return res
          .status(400)
          .send({ status: 0, messege: "user login required" });
      } else {
        userModel
          .findOne({ districtName: req.user.districtName })
          .select({ districtName: 1, _id: 0 })
          .exec((findErr, findRes) => {
            if (findErr) {
              return res
                .status(500)
                .send({ status: 0, messege: "internal server error" });
            } else if (!findRes) {
              return res
                .status(404)
                .send({ status: 0, messege: "district not exist" });
            } else {
              return res
                .status(200)
                .send({
                  status: 1,
                  messege: "district fatch successfully",
                  findRes,
                });
            }
          });
      }
    } catch (error) {
      return res
        .status(409)
        .send({ status: 0, messege: "Temporerty error occured" });
    }
  },
  // this is end of get district
  addChild: (req, res) => {
    try {
      if (!req.user) {
        return res
          .status(400)
          .send({ status: 0, messege: "user login required" });
      } else {
        var data = {
          name: req.body.name,
          sex: req.body.sex,
          fatherName: req.body.fatherName,
          motherName: req.body.motherName,
          dateOfBirth: req.body.dateOfBirth,
          state: req.body.state,
          district: req.body.district,
        };
        new childModel(data).save((saveErr, saveRes) => {
          if (saveErr) {
            return res
              .status(500)
              .send({ status: 0, messege: "internal server error" });
          } else if (!saveRes) {
            return res
              .status(400)
              .send({ status: 0, messege: "error in adding child" });
          } else {
            return res
              .status(200)
              .send({
                status: 1,
                messege: "child added successfully",
                saveRes,
              });
          }
        });
      }
    } catch (error) {
      return res
        .status(409)
        .send({ status: 0, messege: "Temporerty error occured" });
    }
  },
  // This is end of addChild
  viewChild: (req, res) => {
    try {
      if (!req.user) {
        return res
          .status(400)
          .send({ status: 0, messege: "user login required" });
      } else {
        childModel.find({}, (findErr, findRes) => {
          if (findErr) {
            return res
              .status(500)
              .send({ status: 0, messege: "internal server error" });
          } else if (!findRes) {
            return res
              .status(404)
              .send({ status: 0, messege: "child not found" });
          } else {
            return res
              .status(200)
              .send({ status: 0, messege: "child list fatch", findRes });
          }
        });
      }
    } catch (error) {
      return res
        .status(409)
        .send({ status: 0, messege: "temporery error occured" });
    }
  },
  //  this is end of view child list
  logout: (req, res) => {
    try {
      if (!req.user) {
        return res.status(400).send({ status: 0, messege: "login required" });
      } else {
        userModel.findOneAndUpdate(
          { token: req.headers.token },
          { $unset: { token: 1 } },
          (upErr) => {
            if (upErr) {
              return res
                .status(500)
                .send({ status: 0, messege: "internal server error" });
            } else {
              return res
                .status(200)
                .send({ status: 1, messege: "logout successfull" });
            }
          }
        );
      }
    } catch (error) {
      return res
        .status(409)
        .send({ status: 0, messege: "temporery error occured" });
    }
  },
  // This is end of logout api
};
// This is end of module export statement
