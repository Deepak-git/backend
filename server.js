const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const app = express();
require('./dbCon/dbCon')


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(cors());
app.use('/user',require('./router/userRoute'))



app.listen(2001,()=>{
    console.log(`Server is live at 2001`)
})